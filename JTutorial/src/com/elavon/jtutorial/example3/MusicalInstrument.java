package com.elavon.jtutorial.example3;

public interface MusicalInstrument {
	void play();
}