package com.elavon.jtutorial.example3;

public class Band {
	public static void main(String[] args) {
		MusicalInstrument bassGuitar = new Guitar();
		bassGuitar.play();
		
		MusicalInstrument grandPiano = new Piano();
		grandPiano.play();
				
	}
}
