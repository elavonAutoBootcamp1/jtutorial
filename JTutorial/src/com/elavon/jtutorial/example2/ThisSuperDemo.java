package com.elavon.jtutorial.example2;

class Parent {
	void method1() {
		System.out.println("Parent");
	}
}
class Child extends Parent{
	void method1() {
		System.out.println("Child");
	}
	void method2() {
		this.method1();
		super.method1();
	}
}
public class ThisSuperDemo {
	public static void main(String[] args) {
		Child c = new Child();
		c.method2();
	}
}