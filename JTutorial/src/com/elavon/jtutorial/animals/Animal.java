package com.elavon.jtutorial.animals;

public class Animal {
	String specie;
	
	String tellSpecie() {
		return this.specie;
	}
	
	void speak() {
		System.out.println("Animal is peaking");
	}
	
	void move() {
		System.out.println("Animal is moving");
	}
	
	
}
